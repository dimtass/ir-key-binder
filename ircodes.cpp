#include "ircodes.h"

#include <QFile>
#include <QProcess>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QKeyEvent>
#include <QHeaderView>
#include "keyevent.h"


IrCodes::IrCodes(QWidget *parent) : QTableWidget(parent)
{
    init();
    m_enable_capture = false;
}


/**
 * @brief Load IR codes from a file
 * @param fname The filename from where to read codes
 */
void IrCodes::loadCodes(QString fname)
{
    if (QFile::exists(fname)) {
        // open file and load codes
        QFile codes(fname);

        if(!codes.open(QIODevice::ReadOnly)) {
            QMessageBox::information(0, "error", codes.errorString());
        }

        QTextStream in(&codes);
        qInfo() << "Reading codes file (" << fname << ") contents:";
        while(!in.atEnd()) {
            QString line = in.readLine();
            QStringList fields = line.split(",");
            qDebug() << "\t: " << line;

            if (!line.isEmpty()) addCode(fields[0], fields[1], fields[2].toInt());
        }
        codes.close();
    }
}


/**
 * @brief Codes file is ALWAYS saved in "codes.txt"
 */
void IrCodes::saveCodes()
{
    // Save codes
    QFile codes("codes.txt");

    if(!codes.open(QIODevice::WriteOnly)) {
        QMessageBox::information(0, "error", codes.errorString());
    }

    QTextStream out(&codes);
    for (int i=0; i<rowCount(); i++) {
        QTableWidgetItem * row_item = item(i,2);
        out << item(i, 0)->text() << "," << item(i, 1)->text() << ",";
        if (row_item->checkState() == Qt::Unchecked) out << "0";
        else out << "1";
        out << "\r\n";
    }
    codes.close();
    qInfo() << "IR Codes saved";
}


/**
 * @brief Add a new code to the codes table
 * @param ircode The code string to store
 * @param keys  The key sequence
 * @param repeat If code supports repeat
 */
void IrCodes::addCode(QString ircode, QString keys, const int repeat)
{
    //Remove any trailing /r/n
    if (ircode.contains("\r\n")) ircode.remove(ircode.length()-2,2);

    // Skip codes if are already found or are repeated (0xFFFFFFFF)
    if (!m_ir_codes_found.contains(ircode) && !ircode.contains("FFFFFFFF") ) {

        m_ir_codes_found << ircode; // add code to list

        int row = rowCount();
        setRowCount(row+1); // set index to next free row

        // fill columns
        setItem(row, 0, new QTableWidgetItem(ircode));
        setItem(row, 1, new QTableWidgetItem(keys));
        QTableWidgetItem *pItem = new QTableWidgetItem();
        if (!repeat)
            pItem->setCheckState(Qt::Unchecked);
        else
            pItem->setCheckState(Qt::Checked);
        setItem(row, 2, pItem);

        qInfo() << "Added code: " << ircode;

        this->scrollToBottom(); //scroll down
    }
}


void IrCodes::processIR(QString data)
{
    //Remove /r/n
    if (data.contains("\r\n")) data.remove(data.length()-2,2);
    QString ircode = data;
    qDebug() << "IR data: " << data;

    // Check if is a repeat command
    if ((ircode.compare("FFFFFFFF") == 0) && m_last_cmd_repeat) {
        KeyEvent::SendKeybind(m_last_cmd);  // resend last command
        goto exit;  // explicit return
    }

    // Search table for valid ir code
    for (int i=0; i<this->rowCount(); i++) {
        QTableWidgetItem * tbl_code = this->item(i, 0);
        if (tbl_code->text().compare(ircode) == 0) {
            QString keybind = this->item(i,1)->text();
            if (!keybind.isEmpty()) {
                // Check if shortcut is a process to run
                if (keybind.startsWith("proc:")) {
                    QProcess *process = new QProcess(this);
                    QString file = keybind.mid(5);
                    process->start("\"" + file + "\"");
                    qDebug() << "Start process: " << file;
                }
                else {
                    // Send keys
                    KeyEvent::SendKeybind(keybind);
                    // Check for repeat
                    if (this->item(i,2)->checkState() == Qt::Checked) {
                        m_last_cmd = keybind;
                        m_last_cmd_repeat = true;
                        goto exit;
                    }
                }
            }
        }
    }
    // Clear any repeat flags
    m_last_cmd.clear();
    m_last_cmd_repeat = false;

exit:   // only for explicit returns
    return;
}


/**
 * @brief When this signal is set then enter learn mode
 * @param enabled true/false
 */
void IrCodes::captureKeys(bool enabled)
{
    m_enable_capture = enabled;
}


/**
 * @brief Capture table's key events
 * @param e Pointer to key data
 */
void IrCodes::keyPressEvent(QKeyEvent *e)
{
    if (m_enable_capture == false) return;

    Q_UNUSED(e);

    // Translate key sequence to string
    QString keySeq = KeyEvent::SequenceKeyToStr((QKeyEvent *) e);
    if (keySeq != NULL) {
        qDebug() << "New KeySequence:" << keySeq;

        // update key sequence to cell #1
        QTableWidgetItem * item = this->item(currentRow(), 1);
        item->setText(keySeq);
        m_enable_capture = false;   // stop input scanning
        emit keyBindFinished();     // signal to GUI
    }

}


/**
 * @brief Initialize table data
 */
void IrCodes::init()
{
    clear();

    // Create table
    setColumnCount(3);
    setRowCount(0);
    QStringList header;
    header << "IR Code" << "Key binding" << "Repeat";
    setHorizontalHeaderLabels(header);
    horizontalHeader()->setStretchLastSection(true);
    setColumnWidth(0, 100);
    setColumnWidth(1, 250);
    setColumnWidth(2, 50);

    // clear found codes
    m_ir_codes_found.clear();
}


/**
 * @brief Import an existing IR codes file
 */
void IrCodes::importFile()
{
    QString fname = QFileDialog::getOpenFileName(this,
        "Import IR codes", "", "IR code Files (*.txt)");

    qDebug() << "Import file: " << fname;
    init();
    loadCodes(fname);
}


/**
 * @brief Delete all IR codes from table
 */
void IrCodes::deleteAll()
{

    if (QMessageBox::warning(this, tr("Warning"), "This operation will delete all IR codes already found!",
        QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok)
    {
        // Clean all previous codes
        repeat:
        for (int i=0; i<this->rowCount(); i++) {
            QTableWidgetItem * iCode = this->item(i, 0);
            int row = iCode->row();
            if (m_ir_codes_found.contains(iCode->text())) {
                // remove from list
                m_ir_codes_found.removeOne(iCode->text());
            }
            this->removeRow(row);
            goto repeat;
        }
        init();
    }
}


/**
 * @brief Delete ONLY selected IR code from table
 */
void IrCodes::deleteCode()
{
    repeat:
    for (int i=0; i<this->rowCount(); i++) {
        QTableWidgetItem * iCode = this->item(i, 0);
        if (iCode->isSelected()) {
            int row = iCode->row();
            if (m_ir_codes_found.contains(iCode->text())) {
                // remove from list
                m_ir_codes_found.removeOne(iCode->text());
            }
            this->removeRow(row);
            goto repeat;
        }
    }
}


/**
 * @brief Test a command without using remote control
 */
void IrCodes::testCommand()
{
    for (int i=0; i<this->rowCount(); i++) {
        QTableWidgetItem * fld1 = this->item(i, 0);
        QTableWidgetItem * fld2 = this->item(i, 1);
        if (fld1->isSelected() || fld2->isSelected()) {
            processIR(this->item(i, 0)->text());
        }
    }
}


/**
 * @brief Add process to IR command
 */
void IrCodes::addProcess()
{
    QString fname = QFileDialog::getOpenFileName(this,
        "Select process of file", "", "File (*.*)");

    for (int i=0; i<this->rowCount(); i++) {
        QTableWidgetItem * fld1 = this->item(i, 0);
        QTableWidgetItem * fld2 = this->item(i, 1);
        if (fld1->isSelected() || fld2->isSelected()) {
            this->item(i,1)->setText("proc:" + fname);
        }
    }
}
