#ifndef IRCODES_H
#define IRCODES_H

#include <QTableWidget>

class IrCodes : public QTableWidget
{
    Q_OBJECT

public:
    explicit IrCodes(QWidget *parent = 0);
    void loadCodes(QString fname = "codes.txt");
    void init();
    void addCode(QString ircode, QString keys, const int repeat);
    void processIR(QString data);

signals:
    void keyBindFinished();

private slots:
    void deleteCode();
    void deleteAll();
    void saveCodes();
    void captureKeys(bool enabled);
    void importFile();
    void testCommand();
    void addProcess();

protected:
    void keyPressEvent(QKeyEvent *e);

private:
    bool    m_enable_capture;
    QList<QString> m_ir_codes_found;
    QString m_last_cmd;
    bool    m_last_cmd_repeat;
};

#endif // IRCODES_H

