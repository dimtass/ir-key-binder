#include <QKeySequence>
#include <QDebug>
#include "keyevent.h"
#include "assert.h"
#include "Windows.h"
#include "WinUser.h"

QMap<int, int> KeyEvent::InitKeymap() {
    QMap<int, int>map;
    map.insert(Qt::Key_Escape, VK_ESCAPE);
    map.insert(Qt::Key_Tab, VK_TAB);
    map.insert(Qt::Key_Backspace, VK_BACK);
    map.insert(Qt::Key_Return, VK_RETURN);
    map.insert(Qt::Key_Enter, VK_RETURN);
    map.insert(Qt::Key_Delete, VK_DELETE);
    map.insert(Qt::Key_Home, VK_HOME);
    map.insert(Qt::Key_End, VK_END);
    map.insert(Qt::Key_Left, VK_LEFT);
    map.insert(Qt::Key_Up, VK_UP);
    map.insert(Qt::Key_Right, VK_RIGHT);
    map.insert(Qt::Key_Down, VK_DOWN);
    map.insert(Qt::Key_PageUp, VK_PRIOR);
    map.insert(Qt::Key_PageDown, VK_NEXT);
    map.insert(Qt::Key_F1, VK_F1);
    map.insert(Qt::Key_F2, VK_F2);
    map.insert(Qt::Key_F3, VK_F3);
    map.insert(Qt::Key_F4, VK_F4);
    map.insert(Qt::Key_F5, VK_F5);
    map.insert(Qt::Key_F6, VK_F6);
    map.insert(Qt::Key_F7, VK_F7);
    map.insert(Qt::Key_F8, VK_F8);
    map.insert(Qt::Key_F9, VK_F9);
    map.insert(Qt::Key_F10, VK_F10);
    map.insert(Qt::Key_F11, VK_F11);
    map.insert(Qt::Key_F12, VK_F12);
    map.insert(Qt::Key_Plus, VK_OEM_PLUS);
    map.insert(Qt::Key_Comma, VK_OEM_COMMA);
    map.insert(Qt::Key_Minus, VK_OEM_MINUS);
    map.insert(Qt::Key_Equal, VK_OEM_MINUS);
    map.insert(Qt::Key_Period, VK_OEM_PERIOD);
    map.insert(Qt::Key_Semicolon, VK_OEM_1);
    map.insert(Qt::Key_Slash, VK_OEM_2);
    map.insert(Qt::Key_QuoteLeft, VK_OEM_3);
    map.insert(Qt::Key_BracketLeft, VK_OEM_4);
    map.insert(Qt::Key_Colon, VK_OEM_5);
    map.insert(Qt::Key_BracketRight, VK_OEM_6);
    map.insert(Qt::Key_Apostrophe, VK_OEM_7);
    map.insert(Qt::Key_Backslash, VK_OEM_102);

    return map;
}

/**
 * @brief key_map is used to map Qt::Key to virtual keys in windows
 *      as not a;; Qt::Key correspond to correct VK values.
 */
const QMap<int, int> KeyEvent::key_map = InitKeymap();


/**
 * @brief Converts a string key sequence to int
 * @param str Keysequence
 * @return int key code
 */
uint KeyEvent::SequenceStrToInt(QString const & str)
{
    QKeySequence seq(str);
    uint keyCode;

    // We should only working with a single key here
    if(seq.count() == 1)
        keyCode = seq[0];
    else {
        // Should be here only if a modifier key (e.g. Ctrl, Alt) is pressed.
        assert(seq.count() == 0);

        // Add a non-modifier key "A" to the picture because QKeySequence
        // seems to need that to acknowledge the modifier. We know that A has
        // a keyCode of 65 (or 0x41 in hex)
        seq = QKeySequence(str + "+A");
        assert(seq.count() == 1);
        assert(seq[0] > 65);
        keyCode = seq[0] - 65;
    }

    return keyCode;
}


void KeyEvent::SendKey(int keycode, bool press)
{
    INPUT key;
    key.type = INPUT_KEYBOARD;
    key.ki.wScan = 0; // hardware scan code for key
    key.ki.time = 0;
    key.ki.dwExtraInfo = 0;
    key.ki.wVk = keycode;
    key.ki.dwFlags = 0; // 0 for key press
    if (press == false) key.ki.dwFlags = KEYEVENTF_KEYUP;   // KEYEVENTF_KEYUP for key release
    SendInput(1, &key, sizeof(INPUT));
    return;
}

/**
 * @brief Send a key sequence globally on system
 * @param keys The key string sequence
 */
void KeyEvent::SendKeybind(QString keys)
{
    // Get key code w/o the modifiers
    uint keycode = SequenceStrToInt(keys); // Get key code from string

    // Search for Alt, Ctrl, Shift, Meta keys
    // and PRESS
    //Alt
    if (keycode & Qt::AltModifier) {
        SendKey(VK_MENU);
    }
    // Ctrl
    if (keycode & Qt::ControlModifier) {
        SendKey(VK_CONTROL);
    }
    // Shift
    if (keycode & Qt::ShiftModifier) {
        SendKey(VK_SHIFT);
    }
    // Meta
    if (keycode & Qt::MetaModifier) {
        SendKey(VK_LWIN);
    }

    int special_key = key_map[keycode & 0x01FFFFFF];
    if (special_key) {
        SendKey(special_key & 0xFF);
        qDebug() << "Sending special key: 0x" << hex << special_key;
    }
    else {
        SendKey(keycode & 0xFF);
    }
    qDebug() << "Sending keybind: 0x" << hex << SequenceStrToInt(keys);

    // Release special buttons

    //Alt
    if (keycode & Qt::AltModifier) {
        SendKey(VK_MENU, false);
    }
    // Ctrl
    if (keycode & Qt::ControlModifier) {
        SendKey(VK_CONTROL, false);
    }
    // Shift
    if (keycode & Qt::ShiftModifier) {
        SendKey(VK_SHIFT, false);
    }
    // Meta
    if (keycode & Qt::MetaModifier) {
        SendKey(VK_LWIN, false);
    }
    if (special_key) {
        SendKey(special_key & 0xFF, false);
        qDebug() << "Sending special key: 0x" << hex << special_key;
    }
    else {
        SendKey(keycode & 0xFF, false);
    }
}


/**
 * @brief Converts a QKeyEvent to a string key sequence
 * @param e The QKeyEvent
 * @return QString String key sequence
 */
QString KeyEvent::SequenceKeyToStr(QKeyEvent *e)
{
    int keyInt = e->key();
    Qt::Key key = static_cast<Qt::Key>(keyInt);

    if(key == Qt::Key_unknown){
        qDebug() << "Unknown key from a macro probably";
        return(NULL);
    }

    if(key == Qt::Key_Control ||
        key == Qt::Key_Shift ||
        key == Qt::Key_Alt ||
        key == Qt::Key_Meta)
    {
        return(NULL);
    }


    Qt::KeyboardModifiers modifiers = e->modifiers();

    if(modifiers & Qt::ShiftModifier)
        keyInt += Qt::SHIFT;
    if(modifiers & Qt::ControlModifier)
        keyInt += Qt::CTRL;
    if(modifiers & Qt::AltModifier)
        keyInt += Qt::ALT;
    if(modifiers & Qt::MetaModifier)
        keyInt += Qt::META;

    qDebug() << "Key code: " << hex << keyInt;
    qDebug() << "Key code: " << hex << e->nativeVirtualKey();
    qDebug() << "Key code: " << hex << e->nativeScanCode();

    return(QKeySequence(keyInt).toString(QKeySequence::NativeText));
}

