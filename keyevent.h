#ifndef KEYEVENT_H
#define KEYEVENT_H

/***
 * This file contains only static methods that are used as tools
 * to encode, decode and send key presses.
 */

#include <QString>
#include <QKeyEvent>
#include <QMap>

class KeyEvent
{

public:
    static const QMap<int, int> key_map;
    static QMap<int, int> InitKeymap();
    static QString SequenceKeyToStr(QKeyEvent *e);
    static uint SequenceStrToInt(QString const & str);
    static void SendKeybind(QString keys);
    static void SendKey(int keycode, bool press = true);
};

#endif // KEYEVENT_H

