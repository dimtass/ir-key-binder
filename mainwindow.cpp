/****************************************************************************
**
** Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
** Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
** Contact: http://www.qt.io/licensing/
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"

#include <QThread>
#include <QMessageBox>
#include <QScrollBar>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QEvent>
//#include <QWindowStateChangeEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // default buttons enable state
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionQuit->setEnabled(true);
    ui->actionConfigure->setEnabled(true);
    ui->actionLearn->setEnabled(false);
    ui->actionSaveIR->setEnabled(true);
    ui->actionDelete->setEnabled(true);
    ui->actionTest->setEnabled(true);

    // Init serial port
    serial = new QSerialPort(this);
    settings = new SettingsDialog;
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));
    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));

    // IR codes table
    ir = new IrCodes;
    ir->setEnabled(true);
    setCentralWidget(ir);
    ir->loadCodes();
    initActionsConnections();
}


MainWindow::~MainWindow()
{
    delete ir;
    delete settings;
    delete ui;
}


/**
 * @brief Initialize signals and slots
 *
 */
void MainWindow::initActionsConnections()
{
    connect(ui->actionConnect, SIGNAL(triggered()), this, SLOT(openSerialPort()));
    connect(ui->actionDisconnect, SIGNAL(triggered()), this, SLOT(closeSerialPort()));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionConfigure, SIGNAL(triggered()), settings, SLOT(show()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(ui->actionLearn, SIGNAL(triggered()), this, SLOT(learnNewCodes()));
    connect(ui->actionSaveIR, SIGNAL(triggered()), ir, SLOT(saveCodes()));
    connect(ui->actionDelete, SIGNAL(triggered()), ir, SLOT(deleteCode()));
    connect(ui->actionClearAll, SIGNAL(triggered()), ir, SLOT(deleteAll()));
    connect(ui->actionKeyBinding, SIGNAL(triggered(bool)), ir, SLOT(captureKeys(bool)));
    connect(ir, SIGNAL(keyBindFinished()), this, SLOT(keyBindFinished()));
    connect(ui->actionImport, SIGNAL(triggered()), ir, SLOT(importFile()));
    connect(ui->actionTest, SIGNAL(triggered()), ir, SLOT(testCommand()));
    connect(ui->actionProc, SIGNAL(triggered()), ir, SLOT(addProcess()));
}


void MainWindow::keyBindFinished()
{
    ui->actionKeyBinding->setChecked(false);
}


void MainWindow::learnNewCodes()
{
    if (ui->actionLearn->isChecked()) {
        m_ir_mode = IR_MODE_LEARN;
        ui->statusBar->showMessage(tr("Learning new IR codes"));
    }
    else {
        m_ir_mode = IR_MODE_NORMAL;
        ui->statusBar->showMessage(tr("Learning disabled"));
    }
}


void MainWindow::about()
{
    QMessageBox::about(this, tr("About IR Key Bind"),
                       tr("The <b>IR Key Bind</b> example demonstrates how to "
                          "communicate with an UIR/IRman compatible arduino IR"
                          "receiver, capture RC-5 codes, bind keyboard events"
                          "on each code and push event on the system."
                          "It supports WinXP,7,8 and Linux.\r\n"
                          "Jaco (8/2015)"));
}


/**********************
 * Serial Port Commands
 * ********************/

/**
 * @brief Open serial port
 */
void MainWindow::openSerialPort()
{
    serial->setPortName(settings->currentSettings.name);
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if (serial->open(QIODevice::ReadWrite)) {
            ui->actionConnect->setEnabled(false);
            ui->actionDisconnect->setEnabled(true);
            ui->actionConfigure->setEnabled(false);
            ui->actionLearn->setEnabled(true);
            ui->statusBar->showMessage(tr("Connected to %1").arg(settings->currentSettings.name));
            // Send IR and wait for OK response
            QThread::msleep(500);
            serial->write("IR\n");
            m_ir_mode = IR_MODE_NORMAL;
            qDebug() << "Serial port: " << settings->currentSettings.name << " opened";
    } else {
        QMessageBox::critical(this, tr("Error"), serial->errorString());

        ui->statusBar->showMessage(tr("Open error"));
    }
}

/**
 * @brief Close serial port
 */
void MainWindow::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionConfigure->setEnabled(true);
    ui->statusBar->showMessage(tr("Disconnected"));
}


void MainWindow::readData()
{
    // For fast interrupt USB-to-Serial drivers
    // always ensure that \r\n is received otherwise
    // the data will be fragmented
    m_data += serial->readAll();
    if (m_data.contains("\r\n")) {
        if (m_ir_mode == IR_MODE_NORMAL) {
            ir->processIR(QString(m_data));
        }
        else if (m_ir_mode == IR_MODE_LEARN) {
            ir->addCode(QString(m_data), "", false);
            qDebug() << "COM: " << m_data;
        }
        m_data.clear();
    }
}


void MainWindow::writeData(const QByteArray &data)
{
    serial->write(data);
}


void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}

// Leave tray icon for future version, too boring...
//
//void MainWindow::changeEvent(QEvent *event)
//{
//    if (event->type() == QEvent::WindowStateChange)
//    {
//        if (isMinimized() == true)
//            setWindowFlags(windowFlags() | Qt::Tool);
//        else
//            setWindowFlags(windowFlags() & ~Qt::Tool);
//    }

//    return QMainWindow::changeEvent(event);

//}
