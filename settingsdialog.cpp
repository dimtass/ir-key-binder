/****************************************************************************
**
** Copyright (C) 2012 Denis Shienkov <denis.shienkov@gmail.com>
** Copyright (C) 2012 Laszlo Papp <lpapp@kde.org>
** Contact: http://www.qt.io/licensing/
**
** This file is part of the QtSerialPort module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QtSerialPort/QSerialPortInfo>
#include <QIntValidator>
#include <QLineEdit>
#include <QFile>
#include <QSettings>
#include <QDebug>

QT_USE_NAMESPACE

static const char blankString[] = QT_TRANSLATE_NOOP("SettingsDialog", "N/A");

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    connect(ui->applyButton, SIGNAL(clicked()),
            this, SLOT(apply()));
    connect(ui->serialPortInfoListBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(checkCustomDevicePathPolicy(int)));
    fillPortsInfo();
    LoadConfig();
    updateDialog();
    updateSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}


void SettingsDialog::LoadConfig()
{
    // Check if config file exists
    if (QFile::exists("./config.ini")) {
        // Load config file
        QSettings conf("./config.ini", QSettings::IniFormat);

        currentSettings.name = conf.value("Config/com_port").toString();
        currentSettings.load = conf.value("Config/load_startup").toBool();
        currentSettings.hide = conf.value("Config/hide").toBool();

        qDebug() << "Configuration loaded.";
    }
    else {
        // Create new config
        currentSettings.name = ui->serialPortInfoListBox->currentText();
        currentSettings.load = ui->cbxLoadOnStartup->isChecked();
        currentSettings.hide = ui->cbxHide->isChecked();
        SaveConfig();
        qDebug() << "Default configuration loaded.";
    }
}

void SettingsDialog::SaveConfig()
{
    QSettings conf("./config.ini", QSettings::IniFormat);
    conf.setValue("Config/com_port", currentSettings.name);
    conf.setValue("Config/load_startup", currentSettings.load);
    conf.setValue("Config/hide", currentSettings.hide);
    conf.sync();

    qDebug() << "Configuration saved";
}

// Scan for system COM ports and fill the list
void SettingsDialog::fillPortsInfo()
{
    ui->serialPortInfoListBox->clear();
    QString description;
    QString manufacturer;
    QString serialNumber;
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);

        ui->serialPortInfoListBox->addItem(list.first(), list);
    }
}

void SettingsDialog::apply()
{
    updateSettings();
    SaveConfig();
    hide();
}

void SettingsDialog::checkCustomDevicePathPolicy(int idx)
{
    bool isCustomPath = !ui->serialPortInfoListBox->itemData(idx).isValid();
    ui->serialPortInfoListBox->setEditable(isCustomPath);
    if (isCustomPath)
        ui->serialPortInfoListBox->clearEditText();
}

void SettingsDialog::updateDialog()
{
    ui->serialPortInfoListBox->setCurrentText(currentSettings.name);
    ui->cbxLoadOnStartup->setChecked(currentSettings.load);
    ui->cbxHide->setChecked(currentSettings.hide);
}

void SettingsDialog::updateSettings()
{
    currentSettings.name = ui->serialPortInfoListBox->currentText();
    currentSettings.load = ui->cbxLoadOnStartup->isChecked();
    currentSettings.hide = ui->cbxHide->isChecked();
}
