QT += widgets serialport core

TARGET = IR key repeater
TEMPLATE = app
CONFIG += c++11

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    settingsdialog.cpp \
    ircodes.cpp \
    keyevent.cpp

HEADERS += \
    mainwindow.h \
    settingsdialog.h \
    ircodes.h \
    keyevent.h

FORMS += \
    mainwindow.ui \
    settingsdialog.ui

RESOURCES += \
    terminal.qrc

RC_FILE = terminal.rc
